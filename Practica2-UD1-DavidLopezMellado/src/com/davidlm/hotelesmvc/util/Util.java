/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.util;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.io.File;

public class Util {


    /**
     * Método que hará que nos generará un mensaje de diálogo avisándonos de un error y pintándones el string que recibe
     * @param mensaje
     */
    public static void mensajeError(String mensaje){
        JOptionPane.showMessageDialog(null,mensaje,"Error",JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Método que nos generará una ventana con opciones de si o no y nos pintará el mensaje que recibe como parámetro.
     * @param mensaje Mensaje que recibe
     * @param titulo Titulo que recibe
     * @return
     */
    public static int mensajeConfirmacion(String mensaje, String titulo){
        return JOptionPane.showConfirmDialog(null, mensaje, titulo, JOptionPane.YES_NO_OPTION);
    }


    /**
     * Método que nos servirá para adjudicar una ruta por defector a un objeto JFileChooser y también le indicará
     * el tipo de extensión que desea para el fichero.
     *
     * @param rutaDefecto Ruta por defecto
     * @param tipoArchivos Tipo de archivos
     * @param extension Extensión del fichero
     * @return
     */
    public static JFileChooser crearSelectorFicheros(File rutaDefecto, String tipoArchivos, String extension){
        JFileChooser selectorFichero = new JFileChooser();
        if(rutaDefecto!=null){
            selectorFichero.setCurrentDirectory(rutaDefecto);
        }
        if(extension!=null){
            FileNameExtensionFilter filtro = new FileNameExtensionFilter(tipoArchivos,extension);
            selectorFichero.setFileFilter(filtro);
        }
        return selectorFichero;
    }

}
