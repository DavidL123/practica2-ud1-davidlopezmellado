/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.gui;

import com.davidlm.hotelesmvc.base.Cliente;
import com.davidlm.hotelesmvc.base.ClienteHotel;
import com.davidlm.hotelesmvc.base.ClienteRestaurante;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class HotelModelo {

    //Campos de la clase
    private ArrayList<Cliente> listaClientes;

    public HotelModelo(){
        listaClientes = new ArrayList<Cliente>();
    }


    /**
     * Método que nos devuelve el arrayList listaClientes
     * @return
     */

    public ArrayList<Cliente> obtenerClientes(){
        return listaClientes;
    }

    /**
     * Método que recibe una serie de parámetros y nos crea un objeto de tipo ClienteHotel
     *      * y lo añade al ArrayList listaClientes
     *
     * @param nombreApellidos Nombre y apellidos
     * @param lugarNacimiento Lugar de nacimiento
     * @param domicilio Domicilio
     * @param numeroTelefono Número de teléfono
     * @param codigoPostal Código postal
     * @param fechaReserva Fecha de reserva
     * @param precioHabitacionReservada Precio habitación reservada
     */
    public void altaClienteHotel(String nombreApellidos, String lugarNacimiento, String domicilio,
                            int numeroTelefono, int codigoPostal, LocalDate fechaReserva, double precioHabitacionReservada){
        ClienteHotel nuevoClienteHotel= new ClienteHotel(nombreApellidos, lugarNacimiento, domicilio,
                numeroTelefono,codigoPostal,fechaReserva,precioHabitacionReservada);

            listaClientes.add(nuevoClienteHotel);


    }

    /**
     * Método que recibe una serie de parámetros y nos crea un objeto con esos parámetros de la clase
     *      * ClienteRestaurante y lo añade al ArrayList listaClientes
     *
     * @param nombreApellidos Nombre y apellidos
     * @param lugarNacimiento Lugar de nacimiento
     * @param domicilio Domicilio
     * @param numeroTelefono Número de teléfono
     * @param codigoPostal Código postal
     * @param fechaReserva Fecha de reserva
     * @param cantidadMesasReservadas Cantidad de mesas reservadas
     */

    public void altaClienteRestaurante(String nombreApellidos, String lugarNacimiento, String domicilio,
                                       int numeroTelefono, int codigoPostal, LocalDate fechaReserva, int cantidadMesasReservadas){
        ClienteRestaurante nuevoClienteRestaurante = new ClienteRestaurante(nombreApellidos, lugarNacimiento, domicilio,
                numeroTelefono,codigoPostal, fechaReserva,cantidadMesasReservadas);
            listaClientes.add(nuevoClienteRestaurante);

    }

    /**
     * Método que recibe una variable de tipo String comprueba si existe
     * en el ArrayList listaClientes y nos devuelve un valor booleano.
     * @param nombreApellidos
     * @return
     */

    public boolean existeCliente(String nombreApellidos) {
        for (Cliente unCliente:listaClientes) {
            if(unCliente.getNombreApellidos().equals(nombreApellidos)) {
                return true;
            }
        }
        return false;
    }


    /**
     * Método que recibe un fichero de tipo File y nos escribe en él la configuración que hemos considerado
     * conveniente. Lo rellenará con la información del ArrayList listaClientes
     *
     * @param fichero
     * @throws ParserConfigurationException
     * @throws TransformerException
     */
    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        //Añado el nodo raiz - la primera etiqueta que contiene a las demas
        Element raiz = documento.createElement("Clientes");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoCliente = null, nodoDatos = null;
        Text texto = null;

        for (Cliente unCliente : listaClientes) {

            /*Añado dentro de la etiqueta raiz (Vehiculos) una etiqueta
            dependiendo del tipo de vehiculo que este almacenando
            (coche o moto)
             */
            if (unCliente instanceof ClienteHotel) {
                nodoCliente = documento.createElement("Cliente-Hotel");

            } else {
                nodoCliente = documento.createElement("Cliente-Restaurante");
            }
            raiz.appendChild(nodoCliente);

            /*Dentro de la etiqueta vehiculo le añado
            las subetiquetas con los datos de sus
            atributos (matricula, marca, etc)
             */
            nodoDatos = documento.createElement("nombreApellidos");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(unCliente.getNombreApellidos());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("lugarNacimiento");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(unCliente.getLugarNacimiento());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("domicilio");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(unCliente.getDomicilio());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("numeroTelefono");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Cliente)unCliente).getNumeroTelefono()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("codigoPostal");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(String.valueOf(((Cliente)unCliente).getCodigoPostal()));
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-reserva");
            nodoCliente.appendChild(nodoDatos);
            texto = documento.createTextNode(unCliente.getFechaReserva().toString());
            nodoDatos.appendChild(texto);

            /* Como hay un dato que depende del tipo de vehiculo
            debo acceder a él controlando el tipo de objeto
             */
            if (unCliente instanceof ClienteHotel) {
                nodoDatos = documento.createElement("precio-habitacion-reservada");
                nodoCliente.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((ClienteHotel) unCliente).getPrecioHabitacionReservada()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("cantidad-mesas-reservadas");
                nodoCliente.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((ClienteRestaurante) unCliente).getCantidadMesasReservadas()));
                nodoDatos.appendChild(texto);
            }

        }
        /*
        Guardo los datos en "fichero" que es el objeto File
        recibido por parametro
         */
        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }


    /**
     * Método que recibe un objeto de tipo File y nos modificará ese archivo con la configuración que hemos considerado
     * conveniente. Nos creará un nuevo ArrayList y este ArrayList se irá rellenando con la información que lea
     * del fichero.
     *
     *
     * @param fichero
     * @throws ParserConfigurationException
     * @throws IOException
     * @throws SAXException
     */
    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaClientes = new ArrayList<Cliente>();
        ClienteHotel nuevoClienteHotel = null;
        ClienteRestaurante nuevaClienteRestaurante = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoCliente = (Element) listaElementos.item(i);


            if (nodoCliente.getTagName().equals("Cliente-Hotel")) {
                nuevoClienteHotel = new ClienteHotel();
                nuevoClienteHotel.setNombreApellidos(nodoCliente.getChildNodes().item(0).getTextContent());
                nuevoClienteHotel.setLugarNacimiento(nodoCliente.getChildNodes().item(1).getTextContent());
                nuevoClienteHotel.setDomicilio(nodoCliente.getChildNodes().item(2).getTextContent());
                nuevoClienteHotel.setNumeroTelefono(Integer.parseInt(nodoCliente.getChildNodes().item(3).getTextContent()));
                nuevoClienteHotel.setCodigoPostal(Integer.parseInt(nodoCliente.getChildNodes().item(4).getTextContent()));
                nuevoClienteHotel.setFechaReserva(LocalDate.parse(nodoCliente.getChildNodes().item(5).getTextContent()));
                nuevoClienteHotel.setPrecioHabitacionReservada(Double.parseDouble(nodoCliente.getChildNodes().item(6).getTextContent()));

                listaClientes.add(nuevoClienteHotel);
            } else {
                if (nodoCliente.getTagName().equals("Cliente-Restaurante")) {
                    nuevaClienteRestaurante = new ClienteRestaurante();
                    nuevaClienteRestaurante.setNombreApellidos(nodoCliente.getChildNodes().item(0).getTextContent());
                    nuevaClienteRestaurante.setLugarNacimiento(nodoCliente.getChildNodes().item(1).getTextContent());
                    nuevaClienteRestaurante.setDomicilio(nodoCliente.getChildNodes().item(2).getTextContent());
                    nuevaClienteRestaurante.setNumeroTelefono(Integer.parseInt(nodoCliente.getChildNodes().item(3).getTextContent()));
                    nuevaClienteRestaurante.setCodigoPostal(Integer.parseInt(nodoCliente.getChildNodes().item(4).getTextContent()));
                    nuevaClienteRestaurante.setFechaReserva(LocalDate.parse(nodoCliente.getChildNodes().item(5).getTextContent()));
                    nuevaClienteRestaurante.setCantidadMesasReservadas(Integer.parseInt(nodoCliente.getChildNodes().item(6).getTextContent()));

                    listaClientes.add(nuevaClienteRestaurante);
                }
            }


        }
    }




    @Override
    public String toString() {
        return "HotelModelo{" +
                "listaClientes=" + listaClientes +
                '}';
    }
}
