/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.gui;

import com.davidlm.hotelesmvc.base.Cliente;
import com.davidlm.hotelesmvc.base.ClienteHotel;
import com.davidlm.hotelesmvc.base.ClienteRestaurante;
import com.davidlm.hotelesmvc.util.Util;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.*;
import java.util.Properties;

import static com.sun.java.accessibility.util.AWTEventMonitor.addWindowListener;
import static com.sun.java.accessibility.util.SwingEventMonitor.addListSelectionListener;

public class HotelControlador implements ActionListener, ListSelectionListener, WindowListener {

    //Campos de la clase
    private Ventana vista;
    private HotelModelo modelo;
    private File ultimaRutaExportada;

    /*
    Método constructor que recibe dos objetos de las clases: Ventana y Hotelmodelo,
    ejecuta el método cargarDatosConfiguración y los métodos para accionar distintos listener.
     */
    public HotelControlador(Ventana vista, HotelModelo modelo) {
        this.vista=vista;
        this.modelo=modelo;


        try{
        cargarDatosConfiguracion();
        } catch (IOException e) {
            e.printStackTrace();
        }


        addActionListener(this);

        addListSelectionListener(this);
        addWindowListener(this);
    }


    /**
     *
     * Método que servirá para indicar que es lo que tiene que hacer cada botón.
     * Botón "Nuevo":
     * -Comprueba si hay campos vacíos. Mandará un mensaje de aviso en caso de que haya alguno.
     * -Comprueba si existe el cliente introducido a través del nombre. Utiliza el método existeCliente
     * del objeto "modelo".
     * -Detecta que radioButton está seleccionado y en función de cual sea da de alta al cliente de hotel
     * o al cliente de restaurante.
     * -Ejecuta el método limpiarCampos()
     * -Ejecuta el método refrescar()
     *
     * Botón "Importar":
     * -Nos crea un fichero con JFileChooser utilizando la configuración del método .crearSelectorFicheros
     * de la clase Util
     * -Utilizando el comando .showOpenDialog nos aparecerá una venta donde podremos seleccionar un fichero
     * y al pulsar el botón Aceptar lo detectaremos con .APPROVE_OPTION y ejecutaremos el método .importarXML del objeto
     * modelo.
     *
     * Botón "Exportar":
     * -Nos crea un fichero con JFileChooser utilizando la configuración del método .crearSelectorFicheros
     * de la clase Util
     * -Utilizando el comando .showSaveDialog nos aparecerá una venta donde podremos indicarle donde queremos guardar
     * el fichero y al pulsar el botón Aceptar lo detectaremos con .APPROVE_OPTION y ejecutaremos el
     * método .exportarXML del objeto modelo.
     * -Se ejecuta el método actualizarDatosConfiguración()
     *
     * -Botón "Cliente Restaurante":
     * -Modifica un etiqueta seleccionada dándole el valor "Precio Habitación Reservada"
     *
     *
     * -Botón "Cliente Hotel":
     * -Modifica un etiqueta seleccionada dándole el valor "Cantidad Mesas Reservadas "
     *
     *
     *
     * @param e
     */

    @Override
    public void actionPerformed(ActionEvent e) {
        String actionCommand = e.getActionCommand();

        switch (actionCommand){
            case "Nuevo":
                if(hayCamposVacios()){
                    Util.mensajeError("Los siguientes campos no pueden estar vacíos\n"+
                            "NombreYApellidos\nLugar de nacimiento\nDomicilio\nTelefono\nCódigo postal\nFecha reserva\n"+
                    vista.precioHabitacionNMesasReservadasLbl.getText());
                    break;
                }

                if(modelo.existeCliente(vista.nombreApellidosTxt.getText())){
                    Util.mensajeError("Ya existe un cliente con esos nombres y apellidos\n"+
                            vista.nombreApellidosTxt.getText());
                    break;
                }


                if(vista.clienteHotelRadioButton.isSelected()){
                    modelo.altaClienteHotel(vista.nombreApellidosTxt.getText(),vista.lugarNacimientoTxt.getText(),
                            vista.domicilioText.getText(),Integer.parseInt(vista.telefonoTxt.getText()),
                            Integer.parseInt(vista.codigoPostalTxt.getText()),vista.fechaReservaDPicker.getDate(),
                            Double.parseDouble(vista.numeroMesasTxt.getText()));
                }else if(vista.clienteRestauranteRadioButton.isSelected()){
                    modelo.altaClienteRestaurante(vista.nombreApellidosTxt.getText(),vista.lugarNacimientoTxt.getText(),
                           vista.domicilioText.getText(), Integer.parseInt(vista.telefonoTxt.getText()),
                            Integer.parseInt(vista.codigoPostalTxt.getText()),vista.fechaReservaDPicker.getDate(),
                            Integer.parseInt(vista.numeroMesasTxt.getText()));
                }
                limpiarCampos();
                refrescar();
                break;
            case "Importar":

                JFileChooser selectorFichero = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivo XML",
                        "xml");

                //Te saca la ventana para abrir un fichero.
                int opt = selectorFichero.showOpenDialog(null);
                if (opt == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.importarXML(selectorFichero.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    } catch (SAXException ex) {
                        ex.printStackTrace();
                    }
                    refrescar();
                }
                break;
            case "Exportar":
                JFileChooser selectorFichero2 = Util.crearSelectorFicheros(ultimaRutaExportada, "Archivos XML",
                        "xml");
                //Te saca esto para guardar un fichero de diálogo
                int opt2 = selectorFichero2.showSaveDialog(null);
                if (opt2 == JFileChooser.APPROVE_OPTION) {
                    try {
                        modelo.exportarXML(selectorFichero2.getSelectedFile());

                        actualizarDatosConfiguracion(selectorFichero2.getSelectedFile());
                    } catch (ParserConfigurationException ex) {
                        ex.printStackTrace();
                    } catch (TransformerException ex) {
                        ex.printStackTrace();
                    }
                }
                break;
            case "Cliente Restaurante":
                vista.precioHabitacionNMesasReservadasLbl.setText("Cantidad Mesas Reservadas ");
                break;
            case "Cliente Hotel":
                vista.precioHabitacionNMesasReservadasLbl.setText("Precio Habitación Reservada");
                break;
        }
    }


    /**
     * Método que nos devolverá un valor booleano comprobando si unos de las campos que hay para rellenar
     * está vacío.
     *
     * @return
     */
    private boolean hayCamposVacios() {
        if (vista.nombreApellidosTxt.getText().isEmpty() ||
                vista.lugarNacimientoTxt.getText().isEmpty() ||
                vista.domicilioText.getText().isEmpty() ||
                vista.telefonoTxt.getText().isEmpty() ||
                vista.codigoPostalTxt.getText().isEmpty() ||
                vista.numeroMesasTxt.getText().isEmpty() ||
                vista.fechaReservaDPicker.getText().isEmpty()) {
            return true;
        }
        return false;
    }

    /**
     * Método que nos vaciara los campos y pondra el cursor en el campo que tenga puesto .requestFocus();
     */
    private void limpiarCampos(){
        vista.nombreApellidosTxt.setText(null);
        vista.lugarNacimientoTxt.setText(null);
        vista.domicilioText.setText(null);
        vista.numeroMesasTxt.setText(null);
        vista.telefonoTxt.setText(null);
        vista.codigoPostalTxt.setText(null);
        vista.fechaReservaDPicker.setText(null);
        vista.nombreApellidosTxt.requestFocus();
    }

    /**
     * Método que nos vacía las casillas del DefaultListModel
     * y nos lo rellena con el método .obtenerClientes() que nos devuelve el valor
     * del arrayList ListaCLientes.
     *
     *
     */
    private void refrescar() {
        vista.dlmCliente.clear();
        for (Cliente unCliente : modelo.obtenerClientes()) {
            vista.dlmCliente.addElement(unCliente);
        }
    }

    /**
     * Método que nos configura los listener de todos los botones. De esta forma nos avisará de cuando
     * se pulsa un botón.
     * @param listener
     */
    private void addActionListener(ActionListener listener) {
        vista.clienteHotelRadioButton.addActionListener(listener);
        vista.clienteRestauranteRadioButton.addActionListener(listener);
        vista.exportarBtn.addActionListener(listener);
        vista.importarBtn.addActionListener(listener);
        vista.nuevoButton.addActionListener(listener);
    }


    /**
     * Método que nos cargará la configuración de "hoteles.conf"
     *
     * @throws IOException
     */
    private void cargarDatosConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.load(new FileReader("hoteles.conf"));
        ultimaRutaExportada = new File(configuracion.getProperty("ultimaRutaExportada"));
    }


    /**
     * Método que actualiza los datos del archivo ultimaRutaExportada
     *
     *
     * @param ultimaRutaExportada
     */
    private void actualizarDatosConfiguracion(File ultimaRutaExportada){
        this.ultimaRutaExportada=ultimaRutaExportada;
    }


    /**
     * Método que nos guarda la configuración del archivo "hoteles.conf"
     * @throws IOException
     */
    private void guardarConfiguracion() throws IOException {
        Properties configuracion = new Properties();
        configuracion.setProperty("ultimaRutaExportada", ultimaRutaExportada.getAbsolutePath());
        configuracion.store(new PrintWriter("hoteles.conf"), "Datos configuracion clientes");

    }


    /**
     * Método que detecterá cambios en los campos, es interno.
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getValueIsAdjusting()) {
            Cliente clienteSeleccionado = (Cliente) vista.list1.getSelectedValue();
            vista.nombreApellidosTxt.setText((clienteSeleccionado.getNombreApellidos()));
            vista.lugarNacimientoTxt.setText(clienteSeleccionado.getLugarNacimiento());
            vista.domicilioText.setText(clienteSeleccionado.getDomicilio());
            vista.telefonoTxt.setText(String.valueOf((clienteSeleccionado).getNumeroTelefono()));
            vista.codigoPostalTxt.setText(String.valueOf((clienteSeleccionado).getCodigoPostal()));
            vista.fechaReservaDPicker.setDate(clienteSeleccionado.getFechaReserva());

            if (clienteSeleccionado instanceof ClienteHotel) {
                vista.clienteHotelRadioButton.doClick();
                vista.numeroMesasTxt.setText(String.valueOf(((ClienteHotel) clienteSeleccionado).getPrecioHabitacionReservada()));
            } else {
                vista.clienteRestauranteRadioButton.doClick();
                vista.numeroMesasTxt.setText(String.valueOf(((ClienteRestaurante) clienteSeleccionado).getCantidadMesasReservadas()));
            }
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Método que se ejecutará cuando cerremos la ventana, apareciendo una ventana de diálogo
     * @param e
     */
    @Override
    public void windowClosing(WindowEvent e) {
        int resp = Util.mensajeConfirmacion("¿Desea salir de la ventana?", "Salir");
        if (resp == JOptionPane.OK_OPTION) {
            try {
                guardarConfiguracion();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
            System.exit(0);
        }
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }









}
