package com.davidlm.hotelesmvc.gui;

import com.davidlm.hotelesmvc.base.Cliente;
import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;

/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
public class Ventana {
    private JPanel panel1;
    public JFrame frame;
    public JRadioButton clienteHotelRadioButton;
    public JRadioButton clienteRestauranteRadioButton;
    public JTextField nombreApellidosTxt;
    public JTextField lugarNacimientoTxt;
    public JTextField domicilioText;
    public JTextField numeroMesasTxt;
    public JButton nuevoButton;
    public JButton exportarBtn;
    public JButton importarBtn;
    public JLabel precioHabitacionNMesasReservadasLbl;
    public JList list1;
    public DatePicker fechaReservaDPicker;
    public JTextField telefonoTxt;
    public JTextField codigoPostalTxt;


    public DefaultListModel<Cliente> dlmCliente;


    /**
     * Método constructor de ventana que realiza varias confiuraciones del JFrame.
     */
    public Ventana(){


        frame= new JFrame("Hoteles MVC");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        initComponents();
    }

    private void initComponents() {
        dlmCliente=new DefaultListModel<Cliente>();
        list1.setModel(dlmCliente);
    }
}
