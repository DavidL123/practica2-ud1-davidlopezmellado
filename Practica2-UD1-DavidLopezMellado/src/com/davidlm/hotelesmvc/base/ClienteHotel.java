/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.base;

import java.time.LocalDate;

public class ClienteHotel extends Cliente{

    //Campo de la clase
    private double precioHabitacionReservada;


    /**
     * Constructor que no recibe parámetros
     */
    public ClienteHotel(){
        super();
    }

    /**
     * Constructor que recibe varios parámetros y da valor a varias variables
     * @param nombreApellidos nombre y apellidos
     * @param lugarNacimiento lugar de nacimento
     * @param domicilio domicilio
     * @param numeroTelefono Número de teléfono
     * @param codigoPostal Código postal
     * @param fechaReserva Fecha de reserva
     * @param precioHabitacionReservada Precio de la habitación reservada por el cliente
     */
    public ClienteHotel(String nombreApellidos, String lugarNacimiento, String domicilio,
                        int numeroTelefono, int codigoPostal, LocalDate fechaReserva, double precioHabitacionReservada) {
        super(nombreApellidos,lugarNacimiento,domicilio,numeroTelefono,codigoPostal,fechaReserva);
        this.precioHabitacionReservada = precioHabitacionReservada;

    }


    public double getPrecioHabitacionReservada() {
        return precioHabitacionReservada;
    }

    public void setPrecioHabitacionReservada(double precioHabitacionReservada) {
        this.precioHabitacionReservada = precioHabitacionReservada;
    }


    @Override
    public String toString() {

        String cadena="Cliente Hotel: "+
                " \nNombre y apellidos: "+getNombreApellidos()+
                " Lugar de nacimiento: "+getLugarNacimiento()+
                " Domicilio: "+getDomicilio()+
                " Telefono: "+getNumeroTelefono()+
                " Codigo Postal: "+getCodigoPostal()+
                " Fecha de reserva "+getFechaReserva()+
                " Precio habitación reservada: "+this.precioHabitacionReservada;
        return cadena;
    }
}
