/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.base;

import java.time.LocalDate;

public class ClienteRestaurante extends Cliente{

    //Campo de la clase
    private int cantidadMesasReservadas;

    /**
     * Método constructor que no recibe ningún parámetro.
     */
    public ClienteRestaurante() {
        super();
    }

    /**
     * Método constructor que recibe parámetros y da valor a varias variables
     * @param nombreApellidos Nombre y apellidos
     * @param lugarNacimiento Lugar de nacimiento
     * @param domicilio Domicilio
     * @param numeroTelefono Número de teléfono
     * @param codigoPostal Código postal
     * @param fechaReserva Fecha de reserva
     * @param cantidadMesasReservadas Cantidad de mesas reservadas por el cliente
     */
    public ClienteRestaurante(String nombreApellidos, String lugarNacimiento, String domicilio, int numeroTelefono,
                              int codigoPostal, LocalDate fechaReserva, int cantidadMesasReservadas) {
        super(nombreApellidos,lugarNacimiento,domicilio, numeroTelefono, codigoPostal,fechaReserva);
        this.cantidadMesasReservadas = cantidadMesasReservadas;
    }



    public int getCantidadMesasReservadas() {
        return cantidadMesasReservadas;
    }

    public void setCantidadMesasReservadas(int cantidadMesasReservadas) {
        this.cantidadMesasReservadas = cantidadMesasReservadas;
    }



    @Override
    public String toString() {

        String cadena="Cliente Restaurante: "+
                " \nNombre y apellidos: "+getNombreApellidos()+
                " Lugar de nacimiento: "+getLugarNacimiento()+
                " Domicilio: "+getDomicilio()+
                " Telefono: "+getNumeroTelefono()+
                " Codigo Postal: "+getCodigoPostal()+
                " Fecha de reserva "+getFechaReserva()+
                " Cantidad mesas reservadas: "+this.cantidadMesasReservadas;
        return cadena;

    }
}
