/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc.base;

import java.time.LocalDate;


public class Cliente {
    //Campos de la clase
    private String nombreApellidos;
    private String lugarNacimiento;
    private String domicilio;
    private int numeroTelefono;
    private int codigoPostal;
    private LocalDate fechaReserva;

    /**
     * Constructor de la clase cliente que no recibe parámetros.
     */
    public Cliente(){

    }

    /**
     * Constructor de la clase cliente que recibe una serie de parámetros y da valor a las variables
     */
    public Cliente(String nombreApellidos, String lugarNacimiento, String domicilio, int numeroTelefono, int codigoPostal
    , LocalDate fechaReserva){
        this.nombreApellidos=nombreApellidos;
        this.lugarNacimiento=lugarNacimiento;
        this.domicilio=domicilio;
        this.fechaReserva=fechaReserva;
        this.codigoPostal=codigoPostal;
        this.numeroTelefono=numeroTelefono;

    }

    public String getNombreApellidos() {
        return nombreApellidos;
    }

    public void setNombreApellidos(String nombreApellidos) {
        this.nombreApellidos = nombreApellidos;
    }

    public String getLugarNacimiento() {
        return lugarNacimiento;
    }

    public void setLugarNacimiento(String lugarNacimiento) {
        this.lugarNacimiento = lugarNacimiento;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public LocalDate getFechaReserva() {
        return fechaReserva;
    }

    public void setFechaReserva(LocalDate fechaReserva) {
        this.fechaReserva = fechaReserva;
    }

    public int getNumeroTelefono() {
        return numeroTelefono;
    }

    public void setNumeroTelefono(int numeroTelefono) {
        this.numeroTelefono = numeroTelefono;
    }

    public int getCodigoPostal() {
        return codigoPostal;
    }

    public void setCodigoPostal(int codigoPostal) {
        this.codigoPostal = codigoPostal;
    }

    @Override
    public String toString() {
        return "Cliente{" +
                "nombreApellidos='" + this.nombreApellidos + '\'' +
                ", lugarNacimiento='" + this.lugarNacimiento + '\'' +
                ", domicilio='" + this.domicilio + '\'' +
                ", telefono = " + this.numeroTelefono +
                ", codigo postal = "+this.codigoPostal+
                ", fechaReserva=" + this.fechaReserva +
                '}';
    }
}
