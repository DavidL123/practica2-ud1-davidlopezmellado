/**
 * @author David López Mellado
 * @since 1.8
 * @version 1.0
 */
package com.davidlm.hotelesmvc;

import com.davidlm.hotelesmvc.gui.HotelControlador;
import com.davidlm.hotelesmvc.gui.HotelModelo;
import com.davidlm.hotelesmvc.gui.Ventana;

import java.io.IOException;


public class Principal {
    public static void main(String[] args) throws IOException {
        Ventana vista = new Ventana();
        HotelModelo modelo = new HotelModelo();
        HotelControlador controlador = new HotelControlador(vista,modelo);
    }
}

